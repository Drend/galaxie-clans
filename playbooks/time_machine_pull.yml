---
- hosts: "{{ scope }}"
  become: yes
  strategy: linear
  gather_facts: no

  vars:
    time_machine_current_backup_dir: "{{ time_machine_backup_dir }}/{{ time_machine_date_marker }}"

  tasks:
    - setup:
        filter:
        
    - name: Find existing backups
      import_tasks: "{{ playbook_dir }}/inc/_time_machine_find_backups.yml"

    - name: Group found backup profiles
      set_fact:
        time_machine_profiles_to_backup: >-
          {{
            time_machine_profiles_to_backup 
            | default({})
            | combine({
              (item|regex_replace('^backup_profile_', '')): vars.ansible_local[item]
            })
          }}
      loop: "{{ time_machine_backup_profiles }}"

    - name: Create backup data slots
      file:
        path: "{{ time_machine_current_backup_dir }}/{{ item[0].key }}/{{ item[1].name }}"
        state: directory
      loop: "{{ time_machine_profiles_to_backup | dict2items | subelements('value.locations') }}"
      delegate_to: localhost
      become: no

    - name: Stop services
      service:
        name: "{{ item[1] }}"
        state: stopped
      loop: "{{ time_machine_profiles_to_backup | dict2items | subelements('value.services') }}"

    - name: Backup
      synchronize:
        mode: pull
        src: "{{ item[1].path }}"
        dest: "{{ time_machine_current_backup_dir }}/{{ item[0].key }}/{{ item[1].name }}"
        use_ssh_args: yes
        compress: yes
        archive: yes
        rsync_opts: "{{ time_machine_rsync_opts_pull }}"
      loop: "{{ time_machine_profiles_to_backup | dict2items | subelements('value.locations') }}"

    - name: Save restore profile
      copy:
        dest: "{{ time_machine_current_backup_dir }}/restore_profile.yml"
        content: |-
          ---
          {{
            { 'restore_profile': time_machine_profiles_to_backup } | to_nice_yaml
          }}
      delegate_to: localhost
      become: no

    - name: Start services
      service:
        name: "{{ item[1] }}"
        state: started
      loop: "{{ time_machine_profiles_to_backup | dict2items | subelements('value.services') }}"
