::

  mailserver_enable: no


System user owning the virtual mailboxes

::

  postfix_virtual_mailbox_gname: vmail
  postfix_virtual_mailbox_gid: 5000
  postfix_virtual_mailbox_uname: vmail
  postfix_virtual_mailbox_uid: 5000
  postfix_virtual_mailbox_home: /home/vmail
  postfix_virtual_mailbox_shell: /usr/bin/nologin


Path to K/V files olding multiple values

::

  postfix_domain_maps_file: /etc/postfix/mapsdomains
  postfix_transport_maps_file: /etc/postfix/transport
  postfix_virtual_alias_domains_file: /etc/postfix/virtual_domains
  postfix_virtual_alias_maps_file: /etc/postfix/virtual_alias_maps
  postfix_virtual_mailbox_maps_file: /etc/postfix/virtual_mailbox_maps


Configuration values to point at K/V files

::

  postfix_transport_maps: "hash:{{ postfix_transport_maps_file }}"
  postfix_virtual_alias_domains: "hash:{{ postfix_virtual_alias_domains_file }}"
  postfix_virtual_alias_maps: "hash:{{ postfix_virtual_alias_maps_file }}"
  postfix_virtual_mailbox_maps: "hash:{{ postfix_virtual_mailbox_maps_file }}"



K/V content

::

  postfix_transports: []
    # - domain: "example.com"
    #   transport: "smtp:internal.example.com"
  postfix_virtual_aliases: []
    # - alias: "info-alias@example.com"
    #   email: "info@example.com"
  postfix_virtual_mailboxes: []
    # - email: "info@example.com"
    #   recipient: "example.com/info"
  postfix_virtual_mailbox_domains: []
    # - example.com
    # - localhost
  postfix_trust_cidr:
    - "127.0.0.1/32"
    # - 192.168.0.0/16
    # - 10.42.42.0/24
  postfix_virtual_aliases_domains: "{{ postfix_virtual_mailbox_domains }}"
    # - example.com
    # - localhost
  mailserver_services_subdomain: mail
  postfix_smtpd_tls_cert_file: "{{ ssl_certs_dir }}/{{ mailserver_services_subdomain }}.{{ system_base_domain }}.fullchain.crt"
  postfix_smtpd_tls_key_file: "{{ ssl_certs_dir }}/{{ mailserver_services_subdomain }}.{{ system_base_domain }}.key"
  postfix_smtpd_sasl_local_domain: "localhost"
  postfix_mailbox_base: "{{ postfix_virtual_mailbox_home }}"
  postfix_virtual_uid_maps: "static:{{ postfix_virtual_mailbox_uid }}"
  postfix_virtual_gid_maps: "static:{{ postfix_virtual_mailbox_gid }}"
  postfix_virtual_minimum_uid: "{{ postfix_virtual_mailbox_uid }}"
  postfix_virtual_mailbox_limit: "512000000"
  postfix_maximal_queue_lifetime: "5d"
  postfix_minimal_backoff_time: "300s"
  postfix_maximal_backoff_time: "4000s"


OpenDKIM

::

  opendkim_conf_file: "/etc/opendkim.conf"
  opendkim_conf_directory: "/etc/opendkim"
  opendkim_key_directory: "/etc/dkimkeys"
  opendkim_key_table_file: "{{ opendkim_conf_directory }}/key_table"
  opendkim_signing_table_file: "{{ opendkim_conf_directory }}/signing_table"
  opendkim_trusted_hosts_file: "{{ opendkim_conf_directory }}/trusted_hosts"
  opendkim_key_size: 1024
  opendkim_keys: "{{ postfix_virtual_mailbox_domains }}"
  opendmarc_keys: "{{ postfix_virtual_mailbox_domains }}"

  mailserver_spam_report_contact: ~
