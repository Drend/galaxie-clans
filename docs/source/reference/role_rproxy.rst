******************
Role: rproxy
******************
::

  rproxy_enable: no

  rproxy_nginx_stream_conf_dir: "/etc/nginx/streams.conf.d"
  rproxy_nginx_managed_sites: []

  rproxy_nginx_enable_sites: no
