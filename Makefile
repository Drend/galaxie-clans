separator = "----------------------------------------------------------------------"

help:
	@echo $(separator)
	@echo "`tput bold``tput smul`Galaxie Clans Ansible Toolkit`tput rmul``tput sgr0` v0.1"
	@echo $(separator)
	@echo ""
	@echo "  make prepare-debian"
	@echo "    => $(prepare-debian-desc)"
	@echo ""
	@echo "  make prepare-alpine"
	@echo "    => $(prepare-alpine-desc)"
	@echo ""
	@echo "  make env"
	@echo "    => $(env-desc)"
	@echo ""
	@echo "  make clean-env"
	@echo "    => $(clean-env-desc)"
	@echo ""
	@echo "  make doc"
	@echo "    => $(doc-desc)"
	@echo ""
	@echo "  make clean-doc"
	@echo "    => $(clean-doc-desc)"
	@echo $(separator)

prepare-debian-desc = "Prepare a Debian-based linux system for project operations"
prepare-debian:
	@echo $(separator)
	@echo $(prepare-debian-desc)
	@echo $(separator)
	@sudo apt-get install direnv python3 python3-venv

prepare-alpine-desc = "Prepare an Alpine linux system for project operations"
prepare-alpine:
	@echo $(separator)
	@echo $(prepare-alpine-desc)
	@echo $(separator)
	@sudo apk add direnv python3 py3-virtualenv

env-desc = "Build local workspace environment"
env:
	@echo $(separator)
	@echo $(env-desc)
	@echo $(separator)
	pip3 install -U --no-cache pip setuptools wheel
	pip3 install -U --no-cache -r requirements.txt 
	ansible-galaxy collection install -fr requirements.yml

clean-env-desc = "Clean all project dependencies off the workspace"
clean-env:
	@echo $(separator)
	@echo $(clean-env-desc)
	@echo $(separator)
	@rm -rf ${PWD}/.direnv

doc-desc = "Build project static html documentation"
doc:
	@echo $(separator)
	@echo $(doc-desc)
	@echo $(separator)
	@cd docs && make html
	@echo $(separator)
	@echo "Static documentation exported:"
	@echo "  file://${PWD}/docs/build/html/index.html"
	@echo $(separator)

clean-doc-desc = "Clean project static html documentation"
clean-doc:
	@echo $(separator)
	@echo $(clean-doc-desc)
	@echo $(separator)
	@cd docs && make clean

# TODO: ansible-collection release target