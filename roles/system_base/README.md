# System_base

Common system preparation role for all hosts. First role to be applied to host.

## Requirements

Operating system must be one of:

* Debian Stable (buster)

## Variables

* https://chrony.tuxfamily.org/comparison.html