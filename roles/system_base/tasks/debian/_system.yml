---
- name: Set hostname
  hostname:
    name: "{{ system_base_hostname }}"

- name: Add hostname ipv4 to /etc/hosts
  lineinfile:
    dest: "/etc/hosts"
    regexp: >-
      ^{{ ansible_default_ipv4.address }}.+$
    line: >-
      {{ ansible_default_ipv4.address }} {{ system_base_hostname }}
  when: not moleculed

- name: Set timezone
  timezone:
    name: "{{ system_base_timezone }}"

- name: Generate locales
  locale_gen:
    name: "{{ item }}"
    state: present
  loop:
    - "{{ system_base_primary_locale }}"
    - "{{ system_base_secondary_locale }}"
  register: locale_gen_status

- name: Update locale
  command: >-
    /usr/sbin/update-locale LANG={{ system_base_primary_locale }} LANGUAGE={{ system_base_language }}
  when:
    - locale_gen_status.changed

- name: Create ansible directories
  file:
    path: "{{ item }}"
    owner: "{{ system_base_ansible_dirs_owner }}"
    group: "{{ system_base_ansible_dirs_group }}"
    mode: "{{ system_base_ansible_dirs_mode }}"
    state: directory
  loop: "{{ system_base_ansible_dirs }}"

- name: Create ssl-cert group
  group:
    name: "ssl-cert"
    system: yes

- name: Create ssl certificates directory
  file:
    path: "{{ system_base_ssl_certs_dir }}"
    state: directory
    owner: "root"
    group: "ssl-cert"
    mode: 0750
    
- name: Tune sysctl
  sysctl:
    name: "{{ item.name }}"
    value: "{{ item.value }}"
    state: present
    sysctl_set: yes
    reload: yes
  loop: "{{ system_base_sysctl_tuning }}"
  when: not moleculed
