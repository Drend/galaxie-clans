---
- name: Create calendar group
  group:
    name: "{{ calendar_user.group }}"
    gid: "{{ calendar_user.gid }}"

- name: Create calendar user
  user:
    name: "{{ calendar_user.name }}"
    uid: "{{ calendar_user.uid }}"
    home: "{{ calendar_user.home }}"
    group: "{{ calendar_user.group }}"
    groups: "{{ calendar_user.groups }}"
    shell: "{{ calendar_user.shell | default('/bin/false') }}"

- name: Create expected directories
  file:
    path: "{{ item }}"
    owner: "{{ calendar_user.name }}"
    group: "{{ calendar_user.group }}"
    state: directory
  loop: "{{ calendar_expected_directories }}"

- name: Render configuration files
  template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: "{{ calendar_user.name }}"
    group: "{{ calendar_user.group }}"
    mode: 0640
  loop: "{{ calendar_expected_templates }}"
  notify: Restart calendar service

- name: Create user file
  blockinfile:
    path: "/etc/radicale/users"
    insertbefore: "BOF"
    owner: "{{ calendar_user.name }}"
    group: "{{ calendar_user.group }}"
    mode: 0640
    create: yes
    block: |-
      # This file is to be managed by galaxie-clans tooling. Any manual addition can be erased at any time.

- name: "Set calendar passwords"
  htpasswd:
    path: /etc/radicale/users
    name: "{{ item.email }}"
    password: "{{ ansible_local.system_users_password_hash[item.system_user].bcrypt }}"
    crypt_scheme: plaintext
    create: yes
  loop: "{{ postfix_virtual_mailboxes }}"
  when:
    - item.system_user is defined
    - item.system_user in ansible_local.system_users_password_hash.keys() | default([])

- name: Render rproxy service configuration
  template:
    src: "nginx.conf.j2"
    dest: "/etc/nginx/sites-available/calendar.conf"
  notify: Reload nginx service

- name: Pre-pull container images
  docker_image:
    name: "{{ item.value }}"
    source: pull
  loop: "{{ calendar_containers | dict2items }}"

- name: Start calendar service
  systemd:
    name: calendar
    state: started
    daemon_reload: yes
