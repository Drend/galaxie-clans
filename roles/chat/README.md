# Galaxie Clans - L'abre à Palabres

## Introduction
**l'Abre a Palabres** est un service que **Galaxie Family** fait tourner sur ses propres serveurs.
L'idée est de rester en contact avec sa famile et ses proches en cas de rupture de la normalitée.

L'adresse est la suivante: https://palabre.clans.galaxie.family

Ce service ne fonctionne que par invitation.

Nous avons choisi Mattermost pour sa facilité d'installation sur ordinateur et/ou téléphone/tablette.

## Règles de bonne conduite

Ce service est utilisé par plusieurs familles, ainsi que par les proches de ces familles.

* Respect de la hiérachie familiale
* Les mineurs utilisent la plateforme sous la responsabilitée de leurs parents.

## Installation
#### D'un ordinateur
Vous n'avez rien d'autre a faire que de lancer votre navigateur internet et d'entrée l'adresse suivante:
https://palabre.clans.galaxie.family

#### D'un IPHONE ou Android
Allez dans l'**Apple store** ou **Google Play**, recherchez l'application **Mattermost** et installer la.
Pensez également à installer l'application **Jistsi** qui nous est utilise pour les conférences audio ou vidéo..

#### D'un Android via F-Droid
Nous vous conseillons le Store F-Droid qui permet le télécharegemnt d'application mais surtout le partage d'application via SMS/ Bluetouch / Email.
* https://f-droid.org/fr/
* https://f-droid.org/fr/packages/com.mattermost.mattermost/

Liens direct vers les fichier d'installation .apk (au pire du pire)
* https://f-droid.org/FDroid.apk
* https://f-droid.org/repo/com.mattermost.mattermost_399.apk
* https://f-droid.org/repo/org.jitsi.meet_200300.apk

#### Après installation
Au lancement de Mattermost :
* Entrez l'adress suivante: https://palabre.clans.galaxie.family
* Cliquez sur connecter.

## Créer votre salon
Une fois que vous avez été invité , que vous avez créer votre compte, et que vous êtes connecté:

Vous aurez accès au salon "Place du village", qui est un salon public.

Vous pouvez créer des salons privés ou public selon vos besoins.
Gardez à l'esprit que des mineurs et des chefs de familles utilisent ce salon.

## Inviter vos proches

Une fois connecté sur la plateforme, vous pouvez inviter vos proches. Nous n'avons aucune restrictions autre que la taille du serveur.
Notez que le service n'est accessible que par invitation.

<div style="text-align:center"><img src ="https://gitlab.com/Tuuux/galaxie-clans/raw/master/roles/mattermost/files/invite_01.png" /></div>
<BR>
<div style="text-align:center"><img src ="https://gitlab.com/Tuuux/galaxie-clans/raw/master/roles/mattermost/files/invite_02.png" /></div>

Il ne vous reste plus que a partager ce lien. Par E-Mail , SMS, etc ...