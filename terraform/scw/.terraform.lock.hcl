# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/scaleway/scaleway" {
  version = "1.17.2"
  hashes = [
    "h1:Slr1cQIDbnm4KJM7VWJautW1KveoEXxNnkWa85QCdJ4=",
    "zh:3b17eaf1354623b6e84414940ce7ef7346c61dbd917779209687b2c6b755ac8b",
    "zh:3d9b3bc4f01c653920c4429a3de27a73a72bf6bd94d8ad2b6b36c9018122ab3b",
    "zh:50e2438c5021dce912696c2f35b726bd7dc001d7b8823fee4ad8f1d4401f0f79",
    "zh:7e33525d9b2f84b1946065a48645724a507b116e1b88d7bbbd9441a0cc022755",
    "zh:92ea2fe889b1df0ce3fec3656975be9365e23507797adc47688b9021f78c8e15",
    "zh:9f00037fe512c28865e6feb8c0d5ffbe17b51941f4d684fe6e1983a2756af121",
    "zh:cb69571e53d388bfefa8910aafcb0d34122f33ae2ed641632c5ae57df00ecf9c",
    "zh:cc41d20eb8c330efeb88fbf3a22b8ddc6743ccaa815c470b104ca4953c631765",
    "zh:de1a5883643f5569d9c8f396aecbb409077fa45474fdd53b742cbe2287fd59d7",
    "zh:e4b0d44708f989cd804b70bc9e6acbec8c9f557855ba42f99a0013b20a133878",
    "zh:e7d6c4dfcf1c7ee1a5f1f0aacb6b83cad7eeabe9092b6bee1a3182e819f01af0",
  ]
}
